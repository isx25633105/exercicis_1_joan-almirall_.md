<h5 id="exercici-1.">Exercici 1.</h5>
<p>Digues a quina generació d’ordinadors pertanyen aquests elements:</p>
<pre><code>* Vàlvules al buit: Primera generació d'ordinadors (1946-1954)
* Microprocessador: Quarta generació. (1971-) 
* Circuits integrats_: Tercera generació(1964-1971)
* Transistors: Segona generació(1955-1964)
</code></pre>
<h5 id="exercici-2.">Exercici 2.</h5>
<p>Qui està considerat per a molts com l’inventor de la famosa  <em>màquina analítica</em>: el primer ordinador automàtic i programable?<br>
Charles Babbage.<br>
Va aconseguir implementar-la?<br>
Si, va fer un motor analític que permitia sumar, restar, multiplicar i dividir sense necessitat d’intervenció humana.</p>
<h5 id="exercici-3.">Exercici 3.</h5>
<p>Que era/és el disquet (<em>floppy disk</em>)?<br>
Es un element que permet guardar dades digitals.<br>
Quina empresa el va inventar?<br>
El va inventar IBM al any 1968</p>
<h5 id="exercici-4.">Exercici 4.</h5>
<p>A més d’IBM a quina altra empresa li devem en gran mesura l’existència de l’ordinador personal? A principis dels 90 va estar a punt de fer fallida. Avui en dia és HP.<br>
La empresa es diu DEC. ( Digital Equipment Corporation)</p>
<h5 id="exercici-5.">Exercici 5.</h5>
<p>Que deia la llei de Grosch?<br>
Deia que els ordinadors aumentaran al quadrat el seu cost. (ex: Si el ordinador A te un cost de el doble del B. Esperem que A tingui un rendiment quatre vegades superior a B.)</p>
<h5 id="exercici-6.">Exercici 6.</h5>
<p>Quin  <em>mètode</em>  de connexió de xarxes es va crear al centre d’investigació de Xerox al 1973? (i va desmuntar la llei de Grosch)<br>
Es el metode Ethernet, és una tecnológia de xarxa de ordinadors en àrea local.</p>
<h5 id="exercici-7.">Exercici 7.</h5>
<blockquote>
<p>Un projecte finançat per l’Agència de Projectes d’Investigació Avançada en Defensa (ARPA) tenia com a objecte que les comunicacions militars es mantinguessin segures en cas de guerra, quan els trams d’una xarxa podien ser destruïts.</p>
<p>Les primeres xarxes militars que provenien del Projecte Whirlwind tenien unitats de comandament central, i per això era possible atacar al centre de control de la xarxa.</p>
<p>Aquestes unitats es trobaven en edificis sense finestres, reforçats amb estructures de formigó, però si sofrien danys la xarxa deixava de funcionar.</p>
<p>ARPA va finançar la labor d’un grup d’investigadors que van desenvolupar una alternativa en la qual es va dividir la informació en paquets, cadascun els quals rebia l’adreça d’un ordinador receptor i circulaven a través de la xarxa d’ordinadors. Si un o més ordinadors a la xarxa no funcionaven, el sistema trobaria una altra ruta. El receptor reunia els paquets i els convertia en una còpia fidel del document original que havia transmès.</p>
</blockquote>
<p>Que acabava de nèixer?<br>
Internet ( En aquell moment el projecte arpanet)</p>
<h5 id="exercici-8.">Exercici 8.</h5>
<p>Quin conjunt de protocols, que es van crear al CERN, permetien un accès flexible i generalitzat a la informació emmagatzemada a la xarxa en diferents formats?<br>
Van ser els protocols TCP/IP<br>
Qui va ser el seu principal creador?<br>
El seu principal creador vaa ser Tim Berners-Lee.</p>
<h5 id="exercici-9.">Exercici 9.</h5>
<p>Fes un diagrama de l’arquitectura de Von Neumann:<br>
<img src="https://drive.google.com/file/d/1DcAwZuWQEi759CoqUGcv8T39Vjhxjgd0/view?usp=sharing" alt="Diagrama"></p>
<h5 id="exercici-10.">Exercici 10.</h5>
<p>Quin és l’origen de la paraula  <em>informàtica</em>?<br>
El origen la paraula informàtica prove del françes i es el acronim de information y automatique</p>
<p>Links:</p>
<ul>
<li><a href="https://www.bbvaopenmind.com/articulo/historia-de-la-informatica">Història de la informàtica</a></li>
<li><a href="http://asciiflow.com/">Eina per fer diagrames amb format ascii</a></li>
</ul>

